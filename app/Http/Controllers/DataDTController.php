<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelDataDT;

class DataDTController extends Controller
{
	public function index(){
		$usd=ModelDataDT::where('divisa','USD')->get();
		$eur=ModelDataDT::where('divisa','EUR')->get();
		if (count($usd)==0) {
			ModelDataDT::renew();
			return redirect('/'); //inicio
		}
		return view('welcome')->with(['usd'=>$usd,'eur'=>$eur]);
	}
}
