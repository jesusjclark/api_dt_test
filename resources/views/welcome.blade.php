<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Api DollarToday  @jesusjclark</title>
        <!-- scripts -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" >
          $(document).ready(function() {
              $('#tableData').DataTable();
              $('#tableData2').DataTable();
          } );
        </script> 
        <!-- styles -->
        <link href="hhttps://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
      {--   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> --}
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="/css/app.css" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    API DollarToday.
                </div>
                <section>
                    <h3><b>DOLARES</b></h3>

                <div class="box-body table-responsive">
                    <table id="tableData" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                            <tr>
                               <th style="width:10px">#</th>
                               <th>Día</th>
                               <th>Hora</th>
                               <th>DolarToday</th>
                               <th>Implícito</th>
                               <th>DICOM</th>
                               <th>BITCOIN</th>
                               <th>DIPRO</th></b>
                            </tr> 
                        </thead>
                        <tbody>

                        @isset ($usd)
                          @foreach ($usd as $usd)
                            <tr>
                              <td>{{$usd->id}}</td>
                              <td>{{$usd->day}}</td>
                              <td>{{$usd->hour}}</td>
                              <td>{{$usd->decode()->DOLARTODAY}}</td>
                              <td>{{$usd->decode()->IMPLICITO}}</td>
                              <td>{{$usd->decode()->DICOM}}</td>
                              <td>{{$usd->decode()->BITCOIN}}</td>
                              <td>{{$usd->decode()->DIPRO}}</td>
                            </tr>
                          @endforeach  
                        @endisset 
                        </tbody>
                    </table>
                </div>
                <br>
                <h3><b>EUROS</b></h3>
                <div class="box-body table-responsive">
                    <table id="tableData2" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                               <th style="width:10px">#</th>
                               <th>Día</th>
                               <th>Hora</th>
                               <th>DolarToday</th>
                               <th>Implícito</th>
                               <th>DICOM</th>
                               <th>BITCOIN</th>
                               <th>DIPRO</th>
                            </tr> 
                        </thead>
                        <tbody>
                         @isset ($eur)
                          @foreach ($eur as $eur)
                            <tr>
                              <td>{{$eur->id}}</td>
                              <td>{{$eur->day}}</td>
                              <td>{{$eur->hour}}</td>
                              <td>{{$eur->decode()->DOLARTODAY}}</td>
                              <td>{{$eur->decode()->IMPLICITO}}</td>
                              <td>{{$eur->decode()->DICOM}}</td>
                              <td>{{$eur->decode()->BITCOIN}}</td>
                              <td>{{$eur->decode()->DIPRO}}</td>
                            </tr>
                          @endforeach  
                        @endisset  
                        </tbody>
                    </table>
                </div>
                </section>
                <br>
                <div class="links">
                    <a href="https://dolartoday.com/">DollarToday</a>
                    <a href="https://bitbucket.org/jesusjclark/api_dt_test/src/master/">Repositorio Bitbucket</a>
                </div>
            </div>
        </div>
    </body>
</html>
